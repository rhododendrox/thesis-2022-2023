\chapter{CLRS Torch}
\label{ch:clrs_torch}

In questo capitolo presentiamo la libreria CLRS Torch, la versione Pytorch di CLRS sviluppata durante il periodo della tesi.
Il principale obbiettivo di CLRS Torch è rendere CLRS compatibile con PyTorch senza però rimpiazzarlo completamente.
CLRS Torch implementa nuovamente solo ciò che è strettamente incompatibile con PyTorch, per esempio i moduli usati per costruire le reti neurali.
La correttezza dei moduli reimplementati da CLRS Torch è verificata eseguendo i test di unita della libreria originale.
Per tutte le altre componenti viene preservata l'implementazione della libreria originale.
Il codice di CLRS Torch, come per l'originale CLRS, è disponibile pubblicamente\footnote{\url{https://gitlab.com/rhododendrox/clrs-torch}} e rilasciato sotto la licenza Apache 2.0.

\section{Struttura della libreria\label{sec:library_structure}}

Per verificare la correttezza dei propri moduli, CLRS contiene dei test di unità implementati all'interno di file il cui nome termina in \lstinline[style=inline]{_test}.
Essi sono replicati anche all'interno di CLRS Torch per garantire lo stesso grado di correttezza dell'originale CLRS.

CLRS Torch segue la stessa struttura per moduli adottata dalla libreria originale.
Di seguito illustriamo i moduli che compongono CLRS Torch e la loro interfaccia pubblica.

\subsubsection{Probing\label{subsub:module_probing}}

Il modulo \lstinline[style=inline]{probing} contiene la definizione della classe \lstinline[style=inline]{DataPoint} che rappresenta un singolo elemento contenuto in una traiettoria.
Questa classe associa i metadati nome, locazione e tipo all'effettivo tensore contenente il batch di dati.
I metadati sono accessibili tramite le proprietà \lstinline[style=inline]{name}, \lstinline[style=inline]{location} e \lstinline[style=inline]{type_}, mentre il tensore è memorizzato nell'attributo \lstinline[style=inline]{data}.
I campi \lstinline[style=inline]{location} e \lstinline[style=inline]{type_} contengono le locazioni e i tipi descritti nella \cref{sec:data_representation}.
Invece l'attributo \lstinline[style=inline]{name}, il cui valore è unico all'interno di una traiettoria, associa i dati contenuti nel tensore al loro utilizzo all'interno dell'algoritmo.
Per esempio alcuni algoritmi, i quali iterano su i nodi, prevedono un indizio il cui attributo \lstinline[style=inline]{name} ha come valore \lstinline[style=inline]{'i'} e che rappresenta l'indice di iterazione.

Gli oggetti di tipo \lstinline[style=inline]{DataPoint} generati da CLRS non sono compatibili con Pytorch.
Essi contengono nel campo \lstinline[style=inline]{data} valori di tipo \lstinline[style=inline]{jax.Array} o \lstinline[style=inline]{numpy.ndarray}, mentre Pytorch accetta solo valori di tipo \lstinline[style=inline]{torch.Tensor}.
Per facilitare la conversione il modulo \lstinline[style=inline]{probing} implementa la funzione \lstinline[style=inline]{torchify_datapoint} che prende come parametro un \lstinline[style=inline]{DataPoint} e ritorna il risultato della conversione.

\subsubsection{Samplers\label{subsub:module_samplers}}

Una singolo batch di traiettorie è contenuto all'interno di un oggetto di tipo \lstinline[style=inline]{FeedBack} avente due attributi \lstinline[style=inline]{features} e \lstinline[style=inline]{outputs}.
Il campo \lstinline[style=inline]{outputs} contiene una lista di \lstinline[style=inline]{DataPoint} che a loro volta contengono gli output dell'algoritmo.
Il valore dell'attributo \lstinline[style=inline]{features} è, invece, un'oggetto di tipo \lstinline[style=inline]{Features} contente tre campi \lstinline[style=inline]{inputs}, \lstinline[style=inline]{hints} e \lstinline[style=inline]{lengths}.
I campi \lstinline[style=inline]{inputs} e \lstinline[style=inline]{hints} contengono, ciascuno, una tupla di \lstinline[style=inline]{DataPoint} rappresentanti rispettivamente gli input e gli indizi della traiettoria.
Infine l'attributo \lstinline[style=inline]{lengths} contiene la lunghezza di ogni traiettoria del batch.

Dal momento che gli oggetti di tipo \lstinline[style=inline]{Feedback} contengono dei \lstinline[style=inline]{DataPoint} non sono direttamente compatibili con Pytorch e devono essere convertiti.
A questo scopo il modulo \lstinline[style=inline]{samplers} implementa due funzioni \lstinline[style=inline]{torchify_feedback} e \lstinline[style=inline]{torchify_sampler}.

La prima prende come parametro un \lstinline[style=inline]{Feedback}, lo scompone, converte i singoli \lstinline[style=inline]{DataPoint} e ripristina la struttura originale dell'oggetto prima di ritornarlo.

Invece la seconda applica la funzione \lstinline[style=inline]{torchify_feedback} ad ogni valore ritornato da un sampler.
Un sampler è un iteratore che ritorna valori di tipo \lstinline[style=inline]{Feedback}.
Questi \lstinline[style=inline]{Feedback} possono provenire da un dataset generato in precedenza, oppure essere costruiti al momento in cui sono richiesti.
La funzione \lstinline[style=inline]{torchify_sampler} prende come parametro uno di questi iteratori e ne ritorna uno nuovo.
Ogni volta che si chiama la funzione \lstinline[style=inline]{next} sul nuovo iteratore esso: chiama a sua volta la funzione \lstinline[style=inline]{next} sull'iteratore originale ed effettua la conversione del \lstinline[style=inline]{Feedback} ricevuto, ritornandone il risultato.

\subsubsection{Encoders\label{subsub:module_encoders}}

Il modulo \lstinline[style=inline]{encoders} contiene tutto il necessario per costruire ed utilizzare gli encoder.

La funzione \lstinline[style=inline]{construct_encoders} costruisce e ritorna un encoder compatibile con uno specifico tipo di \lstinline[style=inline]{DataPoint}.
Essa ha i seguenti parametri:
\begin{itemize}
  \item \lstinline[style=inline]{stage}: una stringa che indica lo stadio del \lstinline[style=inline]{DataPoint}.
  \item \lstinline[style=inline]{loc}: una stringa che indica la locazione del \lstinline[style=inline]{DataPoint}.
  \item \lstinline[style=inline]{t}: una stringa che indica il tipo del \lstinline[style=inline]{DataPoint}.
  \item \lstinline[style=inline]{hidden_dim}: la dimensione dello spazio latente del processore.
  \item \lstinline[style=inline]{nb_dim}: la dimensione della caratteristica associata al tipo \lstinline[style=inline]{t}. Per esempio nel caso di un \lstinline[style=inline]{DataPoint} di tipo \texttt{categorico} questo parametro rappresenta il numero delle categorie possibili.
  \item \lstinline[style=inline]{init}: una stringa usata per selezionare la strategia di inizializzazione da usare per i parametri dell'encoder. I valori possibili sono \lstinline[style=inline]{'default'} e \lstinline[style=inline]{'xavier_on_scalars'}.
\end{itemize}

La funzione \lstinline[style=inline]{preprocess} prepara un \lstinline[style=inline]{DataPoint} per la fase di codifica.
Per ridurre l'utilizzo di memoria, i \lstinline[style=inline]{DataPoint} di tipo \texttt{puntatore} codificando il loro valore tramite un singolo intero, per poi convertirlo in un vettore one-hot quando richiesto.
La funzione \lstinline[style=inline]{preprocess} effettua questa conversione.

Infine per applicare gli encoder sono implementate le funzioni \lstinline[style=inline]{accum_node_fts},\\ \lstinline[style=inline]{accum_edge_fts} e \lstinline[style=inline]{accum_graph_fts}, per codificare, rispettivamente, le caratteristiche dei nodi, degli archi e del grafo.
Tutte e tre richiedono gli stessi parametri: un'encoder, il \lstinline[style=inline]{DataPoint} da codificare e un tensore nel quale accumulare il risultato.
Sta a chi chiama la funzione assicurarsi che l'encoder sia quello adatto al \lstinline[style=inline]{DataPoint} da codificare.

\subsubsection{Decoders\label{subsub:module_decoders}}

Analogamente al modulo \lstinline[style=inline]{encoders} il modulo \lstinline[style=inline]{decoders} implementa le funzioni usate per creare ed utilizzare i decoder.

La funzione \lstinline[style=inline]{construct_decoders} costruisce e ritorna un decoder compatibile con uno specifico tipo di \lstinline[style=inline]{DataPoint}.
Essa ha i seguenti parametri:
\begin{itemize}
  \item \lstinline[style=inline]{loc}: una stringa che indica la locazione del \lstinline[style=inline]{DataPoint}.
  \item \lstinline[style=inline]{t}: una stringa che indica il tipo del \lstinline[style=inline]{DataPoint}.
  \item \lstinline[style=inline]{hidden_dim}: la dimensione dello spazio latente del processore.
  \item \lstinline[style=inline]{nb_dim}: la dimensione della caratteristica associata al tipo \lstinline[style=inline]{t}.
  \item \lstinline[style=inline]{use_triplets}: un booleano che indica se il processore con il quale il decoder verrà utilizzato fa uso del ragionamento su triplette di nodi.
\end{itemize}

La funzione \lstinline[style=inline]{postprocess} prende i \lstinline[style=inline]{DataPoint} generati dalla fase di decoding e applica su di essi, in base al tipo a cui appartengono, la funzione di attivazione adeguata.
I parametri che \lstinline[style=inline]{postprocess} accetta sono:
\begin{itemize}
  \item \lstinline[style=inline]{spec}: la specifica dell'algoritmo su cui stiamo lavorando.
  \item \lstinline[style=inline]{preds}: i valori da processare. Possono essere degli output, degli indizi oppure entrambi.
  \item \lstinline[style=inline]{sinkhorn_temperature}: il valore di temperatura \(\tau\) da usare con l'operatore di sinkhorn.
  \item \lstinline[style=inline]{sinkhorn_steps}: quanti passi di normalizzazione l'operatore di sinkhorn deve eseguire.
  \item \lstinline[style=inline]{hard}: un booleano che controlla se il post-processing è di tipo hard o soft (come descritto nella \cref{sub:dec}).
\end{itemize}

Infine l'applicazione dei decoder è implementata dalla funzione \lstinline[style=inline]{decode_fts}.
Essa prende in input l'insieme di tutti i decoder, la specifica dell'algoritmo, il nuovo stato generato dal processore, le caratteristiche degli archi e del grafo.
Decodificando le caratteristiche ricevute genera gli output e gli indizi descritti nella specifica.
La funzione \lstinline[style=inline]{decode_fts} non chiama sul proprio risultato la funzione \lstinline[style=inline]{postprocess} lasciando all'utente l'onere.

\subsubsection{Processors\label{subsub:module_processors}}

Il modulo \lstinline[style=inline]{processors} contiene l'implementazione dei processori descritti nella \cref{sub:processor_kinds}.
Tutti i processori estendono la classe astratta \lstinline[style=inline]{Processor} che a sua volta è sottoclasse di \lstinline[style=inline]{torch.nn.Module}.
Ogni processore implementa il metodo \lstinline[style=inline]{forward} in modo che accetti la stessa lista di parametri:
\begin{itemize}
  \item \lstinline[style=inline]{node_fts}: il tensore contente le caratteristiche dei nodi già codificate.
  \item \lstinline[style=inline]{edge_fts}: il tensore contente le caratteristiche degli archi già codificate.
  \item \lstinline[style=inline]{graph_fts}: il tensore contente le caratteristiche del grafo già codificate.
  \item \lstinline[style=inline]{adj_mat}: la matrice di adiacenza del grafo.
  \item \lstinline[style=inline]{hidden}: lo stato generato dalla precedente applicazione del processore. Durante il primo passo contiene tutti zeri.
\end{itemize}

Per costruire un processore è consigliabile fare uso di una ``fabbrica di processori'' ottenibile chiamando la funzione \lstinline[style=inline]{get_processor_factory}.
Questa funzione prende in input:
\begin{itemize}
  \item \lstinline[style=inline]{kind}: una stringa che seleziona il tipo di processore desiderato.
  \item \lstinline[style=inline]{use_ln}: un booleano che controlla se il processore utilizza il layer normalization.
  \item \lstinline[style=inline]{nb_triplet_fts}: dimensione delle caratteristiche da generare durante il ragionamento su triplette di nodi.
    Se il processore richiesto non supporta questa funzionalità il parametro è ignorato.
  \item \lstinline[style=inline]{nb_heads}: numero di teste attenzionali da utilizzare. Il parametro è ignorato se il processore selezionato non usa un meccanismo attenzionale.
\end{itemize}
e ritorna la ``fabbrica'', implementata con una seconda funzione, che accetta come parametro la dimensione dello spazio latente e ritorna il processore.

\subsubsection{Nets\label{subsub:module_nets}}

All'interno del modulo \lstinline[style=inline]{nets} è definita la classe \lstinline[style=inline]{Net} che implementa il \gls{nar} vero e proprio, incapsulando, al sui interno, gli encoder, il processore e i decoder.
Il costruttore della classe \lstinline[style=inline]{Net} accetta i seguenti parametri:
\begin{itemize}
  \item \lstinline[style=inline]{spec}: una lista di specifiche, una per ogni algoritmo da allenare, da cui sono costruiti gli encoder e i decoder.
  \item \lstinline[style=inline]{hidden_dim}: la dimensione dello spazio latente del processore.
  \item \lstinline[style=inline]{encode_hints}: un booleano che controlla se il modello può usare gli indizi o deve fare affidamento solo sui \lstinline[style=inline]{DataPoint} appartenenti allo stadio input.
  \item \lstinline[style=inline]{decode_hints}: un booleano che controlla se durante la fase di decoding sono generati, oltre agli output, gli indizi.
  \item \lstinline[style=inline]{processor_factory}: la ``fabbrica'' utilizzare per costruire il processore.
  \item \lstinline[style=inline]{use_lstm}: se questo parametro è \lstinline[style=inline]{True} un modulo \gls{lstm} è applicato allo stato generato dal processore subito prima della fase di decodifica.
  \item \lstinline[style=inline]{encoder_init}: una stringa usata per selezionare la strategia di inizializzazione da usare per i parametri dell'encoder.
  \item \lstinline[style=inline]{dropout_prob}: il tasso di dropout da applicare allo stato generato dal processore, prima che sia passato al modulo \gls{lstm}.
  \item \lstinline[style=inline]{hint_teacher_forcing}: probabilità con cui gli indizi generati dal modello sono sostituiti con il loro valore atteso contenuto nella traiettoria.
  \item \lstinline[style=inline]{hint_repred_mode}: il tipo di post-processing da effettuare soft o hard. Questo parametro ha significato solo quando \lstinline[style=inline]{encode_hints} e \lstinline[style=inline]{decode_hints} hanno entrambi valore \lstinline[style=inline]{True}.
  \item \lstinline[style=inline]{nb_dims}: un dizionario che contiene per ogni \lstinline[style=inline]{DataPoint} la dimensione associata al suo tipo.
  \item \lstinline[style=inline]{nb_message_passing_steps}: quante volte il processore deve essere applicato prima di ogni fase di decodifica.
\end{itemize}
Una specifica è un dizionario che contiene per ogni nome associato ad un \lstinline[style=inline]{DataPoint} la corrispondente tupla (stadio, locazione, tipo).
Il dizionario Python memorizzato nella variabile \lstinline[style=inline]{clrs.spec.SPECS} contiene le specifiche per ogni algoritmo.
Quando si istanzia un oggetto di tipo \lstinline[style=inline]{Net} il costruttore si occupa di costruire il processore, gli encoder e i decoder ed inizializzare i loro parametri.

Per consentire l'applicazione del modello che essa implementa, la classe \lstinline[style=inline]{Net} sovrascrive il metodo astratto \lstinline[style=inline]{forward}.
Questo metodo prende come parametri:
\begin{itemize}
  \item \lstinline[style=inline]{features}: l'attributo \lstinline[style=inline]{features} della traiettoria sulla quale applicare il \gls{nar}.
  \item \lstinline[style=inline]{repred}: questo booleano deve avere valore \lstinline[style=inline]{True} quando non si sta allenando il modello, \lstinline[style=inline]{False} altrimenti.
  \item \lstinline[style=inline]{algorithm_index}: l'indice dell'algoritmo a cui appartengono le caratteristiche. L'indice è relativo alla lista di specifiche passata al costruttore.
  \item \lstinline[style=inline]{return_hints}: un booleano che controlla se il metodo deve ritornare i valori degli indizi insieme a quelli degli output.
  \item \lstinline[style=inline]{return_all_outputs}: un booleano che controlla se il metodo deve ritornare il valore degli output per ogni passo di esecuzione o solamente per l'ultimo.
\end{itemize}
Questo metodo utilizza il \gls{nar} per eseguire un'algoritmo dall'inizio alla fine su l'intera traiettoria.
Si occupa di chiamare la funzione \lstinline[style=inline]{preprocess}, codifica gli input e gli indizi, applica il processore su gli embeddings il numero richiesto di volte, applica sul risultato del processore il dropout e il modulo \gls{lstm} se richiesto, esegue la fase di decodifica ed applica la funzione \lstinline[style=inline]{postprocess} per generare il risultato.

\subsubsection{Losses\label{subsub:module_losses}}

Il modulo \lstinline[style=inline]{losses} contiene le funzioni \lstinline[style=inline]{output_loss} e \lstinline[style=inline]{hint_loss} utilizzate per calcolare l'errore commesso dal modello.
La prima prende come parametro il valore atteso di un output, la stima del modello del suddetto output e il numero di nodi del grafo.
La seconda, invece, richiede una lista contenente i valori di un indizio per ogni passo di esecuzione ed i valori attesi associati.
Entrambe le funzioni ritornano un numero in virgola mobile rappresentante l'errore cumulativo commesso dal modello.

\subsubsection{Baselines\label{subsub:module_baselines}}

Il modulo \lstinline[style=inline]{baselines} definisce al classe \lstinline[style=inline]{BaselineModel}, la quale combina tutte le funzionalità dei moduli precedenti per semplificare il compito di allenare il modello.
Essa incapsula al suo interno un oggetto di tipo \lstinline[style=inline]{Net} e astrae, nei suoi metodi, una buona parte della procedimento di allenamento, tra cui l'applicazione del modello, il calcolo del gradiente e l'aggiornamento dei parametri.
Il costruttore accetta un soprainsieme dei parametri del costruttore della classe \lstinline[style=inline]{Net} (ad eccezione del parametro \lstinline[style=inline]{nb_dims}) e utilizza i parametri in comune per istanziare un oggetto del suddetto tipo.
I parametri aggiuntivi che accetta sono:
\begin{itemize}
  \item \lstinline[style=inline]{dummy_trajectory}: una lista di traiettorie, una per ogni algoritmo, usata per popolare il dizionario \lstinline[style=inline]{nb_dims} successivamente passato al costruttore di \lstinline[style=inline]{Net}.
  \item \lstinline[style=inline]{learning_rate}: tasso di apprendimento usato durante l'aggiornamento dei parametri.
  \item \lstinline[style=inline]{grad_clip_max_norm}: valore massimo oltre il quale la norma del gradiente non deve andare. Nel caso la norma sia la di sopra di questo valore, il gradiente viene scalato perché il suo magnitudo sia uguale al limite massimo.
  \item \lstinline[style=inline]{checkpoint_path}: directory nella quale effettuare il salvataggio dei parametri.
  \item \lstinline[style=inline]{freeze_processor}: se questo argomento ha valore \lstinline[style=inline]{True} i parametri del processore non sono aggiornati durante la fase di allenamento.
\end{itemize}

Il metodo più importante, all'interno di questa classe, è \lstinline[style=inline]{feedback} il quale implementa un'intero passo di allenamento.
Questo metodo prende in input una traiettoria e l'indice dell'algoritmo da allenare e ritorna l'errore totale commesso dal modello.
Esso applica il modello sulla traiettoria, calcola l'errore totale commesso accumulando i singoli errori ritornati dalle funzioni \lstinline[style=inline]{output_loss} e \lstinline[style=inline]{hint_loss}, deriva da esso il gradiente e infine aggiorna i parametri.

Se si vuole applicare il modello su di una traiettoria senza modificare i parametri si può utilizzare il metodo \lstinline[style=inline]{predict}.
Oltre ad una traiettoria e l'indice dell'algoritmo, esso prende in input due parametri: \lstinline[style=inline]{return_hints} e \lstinline[style=inline]{return_all_outputs} con i quali si può richiedere che il valore di ritorno contenga l'intera serie di indizi e\textbackslash o di output.

Un altro metodo che può tornare utile è \lstinline[style=inline]{compute_grad} il quale accetta gli stessi parametri di \lstinline[style=inline]{feedback} e calcola, ritornandolo, il gradiente senza aggiornare i parametri.
Questo metodo è utilizzabile in coppia con un altro chiamato \lstinline[style=inline]{update_model_params_accum}.
Esso prende come parametro una lista di gradienti calcolati tramite \lstinline[style=inline]{compute_grad}, calcola la media dei loro valori e usa il risultato per eseguire un singolo aggiornamento dei parametri.

Infine \lstinline[style=inline]{BaselineModel} dispone di due metodi, \lstinline[style=inline]{save_model} e \lstinline[style=inline]{restore_model}, per effettuare il salvataggio e il ripristino dei parametri su e da file.
Il primo prende come parametro il nome del file nel quale si vuole salvare l'intero insieme dei parametri.
Il secondo, oltre a richiedere il nome del file dal quale ripristinare i parametri, accetta il parametro booleano \lstinline[style=inline]{only_load_processor}.
Se esso ha valore \lstinline[style=inline]{True} il metodo ripristinerà solo i parametri del processore, invece che dell'intero modello.

\subsubsection{Util\label{subsub:module_util}}

Il modulo \lstinline[style=inline]{util} contiene la funzione di utilità \lstinline[style=inline]{torchify_checkpoint_params}.
Essa permette di convertire un insieme di parametri allenati con CLRS e renderli compatibili con Pytorch.
Il dizionario di parametri convertiti è direttamente utilizzabile come argomento del metodo \lstinline[style=inline]{load_state_dict} della classe \lstinline[style=inline]{Net}.

\section{Utilizzo della libreria\label{sec:library_usage}}

Quello che segue è un semplice esempio che mostra come allenare una rete neurale usando CLRS Torch.
Il procedimento illustrato effettua l'allenamento su di un singolo algoritmo e mantenendo la dimensione del problema fissa.
\'E tuttavia banale adattarlo a più algoritmi e dimensioni del problema.

Il primo passo è importare tutti moduli necessari.
Inoltre è consigliabile inizializzare manualmente i generatori di numeri pseudo-casuali per assicurare la riproducibilità dai risultati.
\begin{lstlisting}[caption=]
import clrs
import clrs_torch
import torch
import numpy as np

# inizializza lo stato generatori di numeri pseudo-casuali
rng = np.random.RandomState(42)
torch.random.manual_seed(rng.randint(2**32))
\end{lstlisting}

Prima di costruire il modello è necessario preparare i dati sui quali sarà allenato.
\'E possibile scegliere se usare un dataset generato in precedenza tramite la funzione \lstinline[style=inline]{create_dataset}, oppure costruire un oggetto di tipo \lstinline[style=inline]{Sampler}, il quale genera un insieme di dati su richiesta, con la funzione \lstinline[style=inline]{build_sampler}.
Entrambe le funzioni ritornano un \lstinline[style=inline]{sampler} che, dopo esser stato convertito in un generatore Python, può essere usato per ottenere degli oggetti di tipo \lstinline[style=inline]{Feedback}.
Un \lstinline[style=inline]{Feedback} contiene un batch di valori di ingresso e valori attesi, memorizzati come coppie traiettoria-risultato.
Inoltre le due funzioni ritornano un dizionario Python (nell'esempio assegnato alla variabile \lstinline[style=inline]{spec}) che contiene i metadati associati ai valori (vedi \cref{sec:data_representation}) contenuti in ogni \lstinline[style=inline]{Feedback}.
\begin{lstlisting}[caption=]
# i dati possono essere caricati dal dataset salvato sul disco

sampler, n_feedbacks, spec = clrs.create_dataset(
  folder='./dataset', # la cartella contenente il dataset
  algorithm='bfs', # l'algoritmo per cui caricare i dati
  batch_size=32, # la dimensione del batch di dati
  split='train' # seleziona i dati per la fase di allenamento
)
sampler = sampler.as_numpy_iterator()

# oppure è possibile generare una serie infinita di Feedbacks

# questa funzione ritorna un generatore di Feedback
def _iterate_sampler(sampler, batch_size):
  while True:
    yield sampler.next(batch_size)

p = tuple([0.1 + 0.1 * i for i in range(9)])

sampler, spec = clrs.build_sampler(
  'bfs', # algoritmo per il quale generare Feedbacks
  seed=rng.randint(2**32),
  num_samples=-1, # nessun limite al numero di Feedbacks
  length=4, # dimensione del problema
  p=p # probabilità che un arco sia inserito nel grafo
)
sampler = _iterate_sampler(sampler, batch_size=32)
\end{lstlisting}

I Feedback, ritornati dai generatori, contengono degli array n-dimensionali implementati da NumPy, i quali non sono direttamente compatibili con PyTorch.
Per facilitare la loro conversione CLRS Torch mette a disposizione la funzione \lstinline[style=inline]{torchify_sampler}.
Essa prende come argomento un generatore e ne ritorna un'altro.
Il nuovo generatore si occupa di effettuare le necessarie conversioni ogni volta che viene richiesto un nuovo valore.

\begin{lstlisting}[caption=]
sampler = clrs_torch.torchify_sampler(sampler)
\end{lstlisting}

Per costruire il modello è sufficiente istanziare la classe \lstinline[style=inline]{BaselineModel}.
Alcuni degli argomenti accettati dal costruttore sono:
\begin{itemize}
  \item \lstinline[style=inline]{spec}: Una lista di specifiche di algoritmi. Da questo argomento dipende il numero e il tipo degli encoder e decoder, e di conseguenza gli algoritmi che il modello potrà apprendere.
\item \lstinline[style=inline]{dummy_trajectory}: Una lista di oggetti \lstinline[style=inline]{Feedback} corrispondenti alle specifiche in \lstinline[style=inline]{spec}. Le forme degli array contenuti nei \lstinline[style=inline]{Feedback} sono usate per determinare la dimensione dei parametri del modello.
  \item \lstinline[style=inline]{processor_factory}: Una funzione che prende come parametro la dimensione dello spazio latente e ritorna un processore compatibile con essa.
  \item \lstinline[style=inline]{hidden_dim}: La dimensione dello spazio latente desiderata.
  \item \lstinline[style=inline]{learning_rate}: Valore per il quale moltiplicare il gradiente prima della sua applicazione.
  \item \lstinline[style=inline]{checkpoint_path}: Cartella nella quale salvare i parametri interni del modello.
\end{itemize}

\begin{lstlisting}[caption=]
# seleziona come processore una Message Passing Neural Network
# con meccanismo di gating e layer normalization
processor_factory = clrs_torch.get_processor_factory(
  'gmpnn',
  use_ln=True
)

model = clrs_torch.models.BaselineModel(
  spec=[spec],
  dummy_trajectory=[next(sampler)],
  processor_factory=processor_factory,
  hidden_dim=128,
  learning_rate=1e-3,
  checkpoint_path='./checkpoints'
)
\end{lstlisting}

Eseguire un passo di allenamento è estremamente semplice, richiedendo la chiamata del singolo metodo \lstinline[style=inline]{.feedback}.
Questo metodo prende come argomenti un \lstinline[style=inline]{Feedback} e l'indice dell'algoritmo per il quale eseguire l'allenamento (relativo alla lista di specifiche passata al costruttore).
All'interno del metodo il modello è applicato alla traiettoria contenuta nel \lstinline[style=inline]{Feedback} e il risultato viene confrontato con l'output atteso.
Successivamente il valore della funzione di loss è usato per calcolare il gradiente, il quale è infine applicato ai parametri del modello.
\begin{lstlisting}[caption=]
step = 0

while step < 100:
  # ottieni un nuovo Feedback
  sample = next(sampler)
  # esegui un passo di allenamento
  loss = model.feedback(
    sample,
    algorithm_index=0
  )

  step += 1
\end{lstlisting}

\section{Esempio di integrazione con Stable Baselines\label{sec:stable_baselines_integration}}

Stable baselines è una libreria che usa Pytorch per implementare vari algoritmi per l'allenamento per rinforzo, tra cui le \gls{dqns} che abbiamo descritto in precedenza (\cref{sub:DQN}).
Di seguito diamo un esempio di come si potrebbe integrare un modello allenato tramite CLRS Torch con Stable Baselines.

Per prima cosa eseguiamo l'import di alcune classi che ci occorrono
\begin{lstlisting}[caption=]
import torch

from clrs_torch._src.nets import Net

from stable_baselines3 import DQN
from stable_baselines3.dqn.policies import QNetwork, DQNPolicy
\end{lstlisting}
tra cui troviamo:
\begin{itemize}
  \item \lstinline[style=inline]{QNetwork}: la rete neurale con cui sono stimati i valori \(Q\) (un \gls{mlp} di default) che vogliamo sostituire.
  \item \lstinline[style=inline]{DQNPolicy}: la classe che sfrutta una \lstinline[style=inline]{QNetwork} per implementare la politica che l'agente segue.
  \item \lstinline[style=inline]{DQN}: la classe che si occupa di calcolare l'errore commesso nella stima dei valori \(Q\) e aggiorna la \lstinline[style=inline]{QNetwork} di conseguenza.
  \item \lstinline[style=inline]{Net}: la rete neurale che vogliamo integrare con Stable Baselines e che sostituirà la \lstinline[style=inline]{QNetwork}.
\end{itemize}

Definiamo una nuova classe \lstinline[style=inline]{CustomQNetwork} estendendo \lstinline[style=inline]{QNetwork} per rendere la nostra rete neurale compatibile con \lstinline[style=inline]{DQNPolicy}.
Nel costruttore usiamo le informazioni riguardo agli stati e alle azioni, ottenibili dai parametri \lstinline[style=inline]{observation_space} e \lstinline[style=inline]{action_space}, per generare una specifica con cui costruire la nostra \lstinline[style=inline]{Net}.
L'implementazione della funzione \lstinline[style=inline]{extract_spec}, usata per generare la specifica, dipende dallo specifico problema in oggetto e non è mostrata in questo esempio.

\begin{lstlisting}[caption=]
class CustomQNetwork(QNetwork):

  def __init__(
    self,
    observation_space
    action_space
    features_extractor
    features_dim
    net_arch = None,
    activation_fn = nn.ReLU,
    normalize_images = True,
  ) -> None:
    super().__init__(
      observation_space,
      action_space,
      features_extractor,
      features_dim,
      net_arch,
      activation_fn,
      normalize_images,
    )

    processor_factory = clrs_torch.get_processor_factory(
      kind="mpnn",
      use_ln=False,
    )

    spec, nb_dims = extract_spec(observation_space, action_space)

    self.q_net = Net(
      spec=[spec],
      hidden_dim=128,
      encode_hints=false,
      decode_hints=false,
      processor_factory=processor_factory,
      use_lstm=False,
      encoder_init='default',
      dropout_prob=0.0,
      hint_teacher_forcing=0.0,
      nb_dims=nb_dims,
    )
\end{lstlisting}

Sovrascriviamo il metodo \lstinline[style=inline]{forward} in modo che, prima di applicare la rete neurale, codifichi le caratteristiche ricevute in input rendendole aderenti alla specifica.
Inoltre le stime generate dalla \lstinline[style=inline]{Net} devono essere convertite nel formato che Stable Baselines si aspetta.
Anche qui \lstinline[style=inline]{convert_features} e \lstinline[style=inline]{convert_predictions} dipendono dal problema che si sta risolvendo e di conseguenza sono omesse.

\begin{lstlisting}[caption=]
  def forward(self, features):
    converted_features = convert_features(features)

    predictions, _ = self.q_net(
      features=converted_features,
      repred=False,
      algorithm_index=0,
      return_hints=False,
      return_all_outputs=False,
    )

    return convert_predictions(predictions)
\end{lstlisting}

Estendiamo la classe \lstinline[style=inline]{DQNPolicy} che faccia uso della nostra \lstinline[style=inline]{CustomQNetwork}.
Il che è facilmente realizzabile sovrascrivendo il metodo \lstinline[style=inline]{make_q_net} perché istanzi la classe corretta.
\begin{lstlisting}[caption=]
class CustomDQNPolicy(DQNPolicy):

  def make_q_net(self):
    return CustomQNetwork(**self.net_args).to(self.device)
\end{lstlisting}

Infine creiamo un oggetto di tipo \lstinline[style=inline]{DQN} fornendo la politica che vogliamo usare e l'ambiente nel quale porre l'agente.
Se abbiamo dei parametri per il processore già allenati, su di un algoritmo che pensiamo possa aiutare l'agente ha raggiungere l'obbiettivo, possiamo caricarli tramite il metodo \lstinline[style=inline]{load_state_dict}.
L'ultimo passo è eseguire l'allenamento con il metodo \lstinline[style=inline]{learn} al quale passiamo il numero di passi di allenamento da eseguire.

\begin{lstlisting}[caption=]
model = DQN(CustomDQNPolicy, env)

model.policy.q_net.q_net.processor.load_state_dict(trained_processor)
model.policy.q_net_target.q_net.processor.load_state_dict(trained_processor)

model.learn(5000)
\end{lstlisting}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis"
%%% End:
