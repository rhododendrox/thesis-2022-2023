\chapter{CLRS Algorithmic Reasoning Benchmark}
\label{ch:clrs}

CLRS \cite{clrs_algorithmic_reasoning_benchmark}, sviluppato dal gruppo DeepMind, vuole essere un banco di prova per valutare il potere di generalizzazione di alcune popolari architetture neurali.
Gli algoritmi utilizzati per questi test coprono varie classi di problemi tra cui: ordinamento di liste, programmazione dinamica, pattern-matching su stringhe e problemi su grafi.
I suddetti algoritmi sono stati scelti tra quelli descritti nel libro ``Introduzione agli algoritmi e strutture dati'' \cite{introduction_to_algorithms} poiché sono alla base di molti programmi e algoritmi più complessi, fornendo un buon punto di partenza e possibilmente componenti combinabili tra loro per modellare nuovi sistemi.
Tra tutti gli algoritmi contenuti nel libro sono stati selezionati solo quelli che soddisfano alcune proprietà.

Deve essere possibile generare gli output in un tempo ragionevole (polinomiale) e con un procedimento affidabile.
Di conseguenza sono stati esclusi i problemi facenti parte della classe NP-hard e gli algoritmi che ne approssimano la soluzione.

Problemi che richiedono una soluzione numerica sono stati esclusi. Valutare la loro performance potrebbe essere ambiguo a seconda della rappresentazione scelta dal modello per i numeri.

Infine, non è stato scelto alcun algoritmo che richiede di allocare dinamicamente della memoria, poiché al momento non è ancora chiaro quale sia il miglior metodo per implementare la suddetta operazione.

Considerate le precedenti restrizioni rimangono i seguenti trenta algoritmi divisi per tipo di problema:

\textbf{Ordinamento}: Insertion sort, bubble sort, heapsort, quicksort \cite{quicksort}.

\textbf{Ricerca}: Ricerca del minimo, ricerca binaria, quickselect \cite{quickselect}.

\textbf{Divide et Impera}: Sottoarray di dimensione massima (versione di Kadane \cite{maximum_subarray}).

\textbf{Greedy}: Activity selection \cite{activity_selection}, schedulazione di attività \cite{task_scheduling}.

\textbf{Programmazione Dinamica}: Matrix chain multiplication, longest common subsequence, albero di ricerca binaria ottimo \cite{design_analysis_computer_algorithms}.

\textbf{Grafi}: Ricerca depth-first and breadth-first \cite{df_bf_search}, ordinamento topologico, articulation points, bridges, algoritmo per le componenti fortemente connesse di Kosaraju \cite{design_analysis_computer_algorithms}, algoritmi per alberi di copertura minimi di Kruskal e Prim \cite{kruskal_min_span_tree, prim_min_span_tree}, algoritmi per i cammini minimi a partire da una sorgente di Bellman-Ford e Dijkstra \cite{bellman_ford, dijkstra} (e corrispondenti versioni su DAG), algoritmi per i cammini minimi per ogni coppia di nodi di Floyd-Warshall \cite{two_pair_shortest_path}.

\textbf{Stringhe}: Na\"ive string matching, Knuth-Morris-Pratt string matcher \cite{KPM_string_matcher}.

\textbf{Geometria}: Intersezione tra segmenti, Algoritmi per inviluppi convessi: metodo di Graham \cite{graham_scan}, cammino di Jarvis \cite{jarvis_march}.

\section{Rappresentazione dei dati\label{sec:data_representation}}

I dati usati per allenare il modello consistono in un insieme di coppie traiettoria e valore atteso.
Una traiettoria è composta da: gli input che rappresentano la specifica istanza del problema da risolvere e una serie di ``indizi'' che catturano l'evoluzione dello stato interno dell'algoritmo.

L'insieme delle operazioni che compongono un'algoritmo possono essere viste come una sequenza di manipolazioni effettuate su di un insieme di elementi e sulle relazioni che esistono tra di essi.
Se l'insieme presenta un ordinamento parziale è possibile esprimere tale relazione dotando gli elementi di un riferimento al proprio predecessore.
Questo modo di vedere gli algoritmi è alla base della rappresentazione basata su grafi proposta da CLRS \cite{clrs_algorithmic_reasoning_benchmark}.

Nella maggior parte dei casi (per esempio quando l'algoritmo non lavora già su grafi) è necessario stabilire che cosa rappresenta un singolo nodo.
Per esempio, nel caso degli algoritmi di ordinamento ogni elemento della lista corrisponde ad un nodo, mentre invece per algoritmi che lavorano su stringhe ogni nodo codifica un singolo carattere.

A tutte le informazioni, associate ai suddetti grafi, vengono affiancati i seguenti metadati che ne descrivono la semantica e la struttura:

\textbf{Stadio:} Ogni elemento di una traiettoria è parte dell'input, dell'output o degli indizi.
A differenza degli input e output, che catturano lo stato di un singolo istante temporale, gli indizi contengono una serie ordinata di stati.

\textbf{Locazione:} Ogni elemento è associato ad uno dei componenti che costituiscono il grafo: i nodi, gli archi oppure il grafo stesso.

\pagebreak

\textbf{Tipo:} Ogni elemento appartiene ad uno di cinque tipi, ognuno con un metodo per la codifica/decodifica e una funzione di loss associati:

\begin{itemize}

\item \texttt{scalare}: Un valore numerico in virgola mobile

\item \texttt{categorico}: Un valore categorico su \(K\) possibili classi.

\item \texttt{mask}: Un valore categorico su due possibili classi.

\item \texttt{mask\_one}: Un valore categorico su due possibili classi espresso tramite una vettore one-hot di lunghezza 1.

\item \texttt{puntatore}: Un valore categorico su tutti gli \(n\) nodi del grafo.

\end{itemize}

La combinazione di locazione e tipo determina la dimensionalità del dato.
Di conseguenza i dati situati nei nodi, archi e nel grafo hanno le rispettive dimensionalità:
\(n \times f\), \(n \times n \times f\), \(f\) dove \(n\) è il numero di nodi nel grafo e \(f\) è determinato dal tipo del dato. (sono omesse la dimensione di batch e, nel caso degli indizi, la dimensione temporale).

\section{Indizi\label{sec:hints}}

Gli algoritmi sono spesso decomponibili in procedure più piccole e riutilizzabili in altri contesti.
Osservare una simile decomposizione all'interno di una rete neurale significa che essa non si sta servendo soltanto dei pattern presenti nei dati per produrre la soluzione, ma ha effettivamente imparato ad eseguire l'algoritmo presentatole.

Sebbene sia in teoria possibile che una rete neurale apprenda come decomporre un algoritmo in autonomia, servendosi soltanto di input e output, in pratica è improbabile che raggiunga un grado di decomposizione accettabile e una buona generalizzazione \gls{ood} senza un aiuto esterno.

Contrariamente, scomponendo il problema originale in sottoproblemi è possibile allenare un modello separato per ognuno di essi, per poi comporre il loro output ottenendo la soluzione.
Incorporare la composizionalità dell'algoritmo direttamente nella struttura del modello garantisce: una generalizzazione \gls{ood} perfetta e la possibilità di riutilizzare i parametri appresi dai sottomodelli per eseguire algoritmi che presentano lo stesso sottoproblema \cite{NEE_learning_execute_subroutines}.
Purtroppo, nel momento in cui il problema preso in considerazione non si presti ad essere facilmente decomposto applicare questo approccio diventa complicato.

CLRS adotta una terza strategia che prova a combinare i punti di forza delle due precedenti.

Gli algoritmi specificano un singolo modo di risolvere un problema, ma non necessariamente l'unico.
Per esempio, nel caso dell'ordinamento di una lista, partendo dallo stesso input, ogni algoritmo produrrà il medesimo risultato.
Per modellare un specifico algoritmo è necessario catturare l'evoluzione dello suo stato interno e di conseguenza il procedimento che utilizza.
Questo è il compito degli indizi, fornire le informazioni necessarie per apprendere, non una strategia di risoluzione qualunque, ma quella descritta dall'algoritmo in oggetto.

Ragionevolmente ci aspettiamo che una rete neurale che abbia appreso come decomporre un algoritmo generi una sequenza di stati simile al suddetto.

Ad ogni passo il modello genera un insieme di indizi, idealmente uguali a quelli contenuti nel dataset, che costituiscono parte dell'input al passo successivo.
Gli indizi così predetti sono poi utilizzati, insieme all'output vero e proprio, per il calcolo della funzione di loss e di conseguenza del gradiente impiegato per l'aggiornamento dei parametri.
In questo modo si favorisce l'apprendimento del procedimento corretto e della sua decomposizione mantenendo, tuttavia, un modello unico ed eliminando la necessità di decomporre il problema manualmente.

\section{Generazione dei dati\label{sec:data_generation}}

Ogni traiettoria è creata a partire da un'istanza randomica del problema, la cui rappresentazione rispetta le specifiche richieste (vedi \ref{sec:data_representation}).
L'istanza viene successivamente risolta dall'algoritmo pertinente, così da ottenere l'output atteso e gli indizi.

Più nello specifico, dato un algoritmo \(A\), una traiettoria viene così generata:

\begin{enumerate}

\item Viene selezionata la dimensione del problema, \(4 \leq n \leq 16\).
  Si possono utilizzare tutti i valori a rotazione oppure sceglierne uno randomicamente.
  Nel caso di algoritmi su stringhe (Na\"ive string matching e Knuth-Morris-Pratt string matcher) \(n\) ha sempre valore 20, e ciò che varia in dimensione sono l'ago e il pagliaio. % MAYBE: is better to use the original names (needle & haystack)

  \item Viene selezionata una probabilità di connessione casuale \(p \in [0,1]\).
    Per evitare traiettorie troppo lunghe, durante l'esecuzione degli algoritmi: articulation points, bridges e ACM Kruskal l'intervallo viene dimezzato e quindi si ha \(p \in [0, 0.5]\).

  \item Si genera l'input, codificato tramite un grafo avente \(n\) nodi, e che contiene valori randomicamente campionati e conformi alle specifiche dell'algoritmo \(A\).

  \item Se \(A\) è un algoritmo su grafi gli archi vengono disposti secondo il modello di Erd\~os-Rényi \cite{erdos_renyi}.
    Per ogni coppia di nodi \((u, v)\) viene aggiunto un arco che gli collega con probabilità \(p\).

  \item Se \(A\) è un algoritmo di pattern-matching su stringhe, si sceglie randomicamente la lunghezza del pattern da cercare \(1 \leq m \leq \lfloor \frac{n}{2} \rfloor\).
    Vengono poi utilizzati \(n - m\) nodi per rappresentare la stringa nella quale si effettua la ricerca (il pagliaio) e i restanti \(m\) contengono la sequenza da cercare (l'ago).

  \item L'algoritmo \(A\) è eseguito sull'input, ottenendo come output la soluzione al problema, e in contemporanea registrando gli stati intermedi.
    Combinando input, stati intermedi (indizi) e output si va a formare una traiettoria completa.

\end{enumerate}

\`E importante notare che praticamente tutti i parametri utilizzati per generare l'input sono randomici o comunque non hanno un valore fisso.
Così facendo si cerca di scoraggiare il modello a fare troppo affidamento su alcune proprietà del grafo, come il numero di nodi, la lunghezza dell'algo e del pagliaio oppure la cardinalità dei vicinati.

Un'altro aspetto fondamentale è la possibilità di controllare la distribuzione dei valori che compongono l'input e dei parametri usati per generarlo. Allenando la rete neurale su dati distribuiti in un certo modo per poi testarla, non solo su dati che non ha mai visto, ma anche che si allontanano di molto dalla distribuzione a cui è abituata, permette di valutare con che grado il modello esegua un vero e proprio algoritmo.
Nel controllare la distribuzione degli input viene posta un'enfasi particolare sulla dimensione del problema, poiché rendere il modello indipendente da essa è uno dei principali obbiettivi.
Un algoritmo che è stato provato può risolvere problemi di qualunque dimensione, mentre invece, nel caso delle reti neurali, le performance tendono a degradarsi quando il valore di \(n\) si discosta troppo da quello usato durante l'allenamento.

I dati possono essere generati \textit{on-the-fly}, fornendo una serie virtualmente infinita di traiettorie, oppure salvati all'interno di un dataset statico.

Durante la fase di training le traiettorie sono generate \textit{on-the-fly} tramite il procedimento sopra descritto.
Per selezionare la dimensione del problema viene definita una sequenza di valori (sempre compresi tra 4 e 16) che vengono usati a rotazione, mentre per gli algoritmi su stringhe \(n\) è fissato a 20.
Ogni batch è composto da traiettorie contenenti lo stesso numero di nodi.

Il modello viene valutato periodicamente su un numero prefissato di input con \(n = 16\) (\(n = 20\) per algoritmi su stringhe).

Infine viene testata la potenza di generalizzazione \gls{ood} del miglior modello utilizzando traiettorie di dimensione \(n = 64 \) (\(n = 80\) per algoritmi su stringhe) prese da un dataset pre-generato\footnote{\url{https://storage.googleapis.com/dm-clrs/CLRS30_v1.0.0.tar.gz}}.

\section{Modelli proposti\label{sec:models}}

% MAYBE: extend this section introduction

Tutte le architetture usate in CLRS sono basate sul paradigma encode-process-decode \cite{enc-proc-dec-paradigm}.
Ogni modello è composto da un insieme di coppie encoder-decoder, ognuna associata ad uno specifico algoritmo, e un singolo processore che è invece condiviso.
Un singolo passo di esecuzione di un generico algoritmo \(\tau\) è equivalente alla composizione di funzioni \(g_\tau \ \circ \ P \ \circ \ f_\tau\) dove \(P\) è il processore e \(g_\tau\) e \(f_\tau\) sono rispettivamente il decoder e l'encoder associati all'algoritmo.

\subsection{Encoder\label{sub:enc}}

All'inizio del \(t\)-esimo passo dell'algoritmo \(\tau\), l'encoder \(f_\tau\), composto da uno strato lineare per ogni input e indizio, proietta gli input e gli indizi pertinenti al passo corrente, in uno spazio ad alta dimensionalità.
Chiamiamo i vettori generati da questa operazione embeddings.
Successivamente gli embeddings situati nei nodi sono sommati tra di loro e lo stesso accade per quelli situati negli archi e nel grafo.
Il risultato finale della fase di encoding è un insieme di embeddings \(\{ \bm{x}_i^{(t)}, \bm{e}_{ij}^{(t)}, \bm{g}^{(t)}\}\) con dimensionalità \( n \times h \), \( n \times n \times h \) e \( h \), rispettivamente per nodi, archi e grafo\footnote{dove \(h\) indica la dimensione dello spazio latente}.

Da notare che la dimensionalità degli embeddings non dipende dal numero e tipo né degli input né degli output.
In questo modo il modello può sfruttare lo stesso spazio latente per eseguire tutti e trenta gli algoritmi.

Inoltre, è importante specificare che gli input non sono processati solo durante il primo passo, ma sono ricodificati ogni volta.
Nel caso di traiettorie particolarmente lunghe la rete neurale potrebbe ``dimenticare'' parte del problema che deve risolvere.
Ricodificando gli input rendiamo disponibili tutte le informazioni necessarie per ricostruire lo stato perso o perturbato.
Questa tecnica è ispirata al modo in cui gli esseri umani risolvono i problemi.
Spesso troviamo utile fermarci e rileggere il testo, magari facendo più attenzione a particolari sezioni del problema alla luce del nostro ragionamento preliminare \cite{recall-mechanism}.

% MAYBE: talk about xavier on scalars

\subsection{Processore\label{sub:processor}}

Il processore \(P\) è dunque applicato agli embeddings, generati durante la fase di encoding, per eseguire un passo di esecuzione dell'algoritmo.
Dal momento che i dati sono codificati tramite un grafo la maggior parte dei processori sono composti da \gls{gnns} \cite{GNNs}.

Generalmente ad ogni sua applicazione il processore calcola un nuovo stato interno dei nodi \(\bm{h}_i^{(t)}\), prendendo come input gli embeddings e lo stato precedente \(\bm{h}_i^{(t-1)}\).
Per ogni arco \((i, j)\) viene generato un messaggio \(\bm{m}_{ij}\) (dove \(j\) è il mittente e \(i\) il destinatario) tramite una funzione apposita \(f_m\)\footnote{\(f_m : \mathbb{R}^{2h} \times \mathbb{R}^{2h} \times \mathbb{R}^{h} \times \mathbb{R}^{h} \rightarrow \mathbb{R}^{h}\)}.
Successivamente, per ogni nodo \(i\), tutti i messaggi provenienti dal vicinato \(\mathcal{N}_i\) sono aggregati tramite una funzione invariante alla permutazione \(\bigoplus\).
Infine una funzione di lettura \(f_r\)\footnote{\(f_r : \mathbb{R}^{2h} \times \mathbb{R}^{h} \rightarrow \mathbb{R}^{h}\)} usa i messaggi aggregati per generare il nuovo stato \(\bm{h}_i^{(t)}\).
\begin{equation}
  \label{eq:processor_gnn}
  \begin{gathered}
    \bm{z}_i^{(t)} = \bm{x}_i^{(t)}||\bm{h}_i^{(t-1)} \qquad \bm{m}_{ij}^{(t)} = f_m\left(\bm{z}_i^{(t)}, \bm{z}_j^{(t)}, \bm{e}_{ij}^{(t)}, \bm{g}^{(t)}\right)\\
    \bm{m}_i^{(t)} = \underset{j \in \mathcal{N}_i}{\bigoplus} \bm{m}_{ij}^{(t)} \qquad \bm{h}_i^{(t)} = f_r\left(\bm{z}_i^{(t)}, \bm{m}_i^{(t)}\right)
  \end{gathered}
\end{equation}
Dove \(||\) denota la concatenazione tra tensori e durante il primo passo \(\bm{h}^{(0)} = 0\).

I valori emessi dal processore sono poi normalizzati perché abbiano media uguale a zero e deviazione standard pari a uno.
In questo modo si cerca di ridurre il fenomeno dello ``spostamento interno della covarianza'' che si verifica quando la distribuzione dei valori di input di uno strato cambia e di conseguenza anche la distribuzione degli input di tutti gli strati successivi.
Arginare questo fenomeno si traduce, spesso, in una riduzione del tempo richiesto perché li modello converga e una maggior potenza di generalizzazione.
Dal momento che il processore viene applicato ad una serie temporale di valori, la cui lunghezza non ha un limite superiore, non è possibile usare il batch normalization \cite{batch_norm}.
Viene invece impiegato il layer normalization \cite{layer_norm} che, invece di eseguire la normalizzazione al livello del batch, normalizza ogni vettore separatamente calcolando media e deviazione standard delle sue componenti.
Questa operazione è così formulata:
\begin{equation}
  \begin{gathered}
    \widehat{\bm{h}}^{(t)}_i = \frac{\bm{g}}{\sigma^{(t)}} \odot \left(\bm{h}^{(t)}_i - \mu^{(t)}\right) + \bm{b} \vspace{10pt}\\
    \mu^{(t)} = \frac{1}{h} \sum_{k=1}^{h} h^{(t)}_{ik} \qquad
    \sigma^{(t)} = \sqrt{\frac{1}{h} \sum_{k=1}^{h} \left(h^{(t)}_{ik} - \mu^{(t)}\right)^2 + \epsilon}
  \end{gathered}
\end{equation}
dove \(\odot\) indica la moltiplicazione rispetto ai singoli elementi del vettore, \(\bm{g}\) e \(\bm{b}\) sono due parametri apprendibili che rispettivamente scalano e traslano il valore e \(h\) è la dimensione dello spazio latente del processore.

\subsection{Tipi di processore\label{sub:processor_kinds}}

% MAYBE: extend this introduction

Quando non meglio specificato viene assunto che il grafo sia una cricca, ovvero ogni nodo è connesso a tutti gli altri \(\left(\mathcal{N}_i = \{1,2,\ldots,n\}\right)\).

\textbf{Graph Attention Networks} \cite{GATs, GATv2}
dove ogni messaggio è generato solamente in funzione delle informazioni situate nel nodo mittente \(f_m(\bm{z}_i, \bm{z}_j, \bm{e}_{ij}, \bm{g}) = \bm{W}\bm{z}_j + \bm{b}\),
e come funzione di aggregazione \(\bigoplus\) viene usata una forma di masked self-attention \cite{attention_is_all}.
Per ogni coppia di nodi viene calcolato un coefficiente di attenzione \(s_{ij}\) che indica l'importanza del nodo \(j\) rispetto al nodo \(i\).
Per prima cosa un singolo strato lineare computa una rappresentazione della coppia di nodi \((i, j)\), alla quale viene poi applicato un meccanismo attenzionale, composto da un altro strato lineare seguito dalla funzione LeakyReLU:
\begin{equation}
  s_{ij} = \text{LeakyReLU}\left(\bm{a}^\intercal \cdot \left(\bm{W}\left[\bm{z}_i||\bm{z}_j||\bm{e}_{ij}||\bm{g}\right] + \bm{b}\right)\right)
\end{equation}
Per rendere i coefficienti confrontabili tra vicinati diversi, essi vengono normalizzati tramite la funzione softmax:
\begin{equation}
  \alpha_{ij} = \text{softmax}_j(s_{ij}) = \frac{\exp\left(s_{ij}\right)}{\sum_{k \in \mathcal{N}_i} \exp\left(s_{ik}\right)}
\end{equation}
Infine i coefficienti \(\alpha_{ij}\) sono impiegati per calcolare una media pesata dei messaggi ricevuti dal vicinato \(\mathcal{N}_i\):
\begin{equation}
  \bm{h}_i = \sum_{j \in \mathcal{N}_i} \alpha_{ij} \left(\bm{W}\bm{z}_j + \bm{b}\right)
\end{equation}
Il modello descritto fino ad ora è l'originale GAT \cite{GATs} il quale ha, tuttavia, ha un potere espressivo limitato.
Infatti è dimostrabile \cite[Teorema 1]{GATv2} che l'architettura originale calcola un'attenzione di tipo statico \cite[Definizione 3.1]{GATv2}, ovvero, l'ordinando dei nodi chiave \(j\), secondo i coefficienti \(\bm{s}_{ij}\), è uguale per ogni nodo query \(i\).
Di conseguenza risulta difficile modellare situazioni in cui chiavi diverse hanno gradi di importanza diversi per nodi query diversi.
Per permettere al modello di calcolare la versione dinamica dell'attenzione \cite[Definizione 3.2]{GATv2} basta scambiare l'ordine delle operazioni, applicando la LeakyReLU prima di calcolare il prodotto scalare con \(\bm{a}^\intercal\):
\begin{equation}
  s_{ij} = \bm{a}^\intercal \cdot \left(\text{LeakyReLU}\left(\bm{W}\left[\bm{z}_i||\bm{z}_j||\bm{e}_{ij}||\bm{g}\right] + \bm{b}^1\right)\right) + \bm{b}^2
\end{equation}
Questa semplice modifica è alla base della migliorata GATv2 \cite{GATv2}.

\textbf{Pointer Graph Networks} \cite{PGNs} dove \(\mathcal{N}_i\) è calcolato a partire dai valori \texttt{puntatore} situati nei nodi e di tipo \texttt{mask} degli archi.
Il grafo è costruito per essere non orientato, quindi se un valore indica che l'arco \(i, j\) dovrebbe essere presente allora verrà aggiunto anche l'arco \(j, i\).
Questo obbliga il modello a ragionare solamente sugli archi definiti dall'istanza del problema che si sta risolvendo (contenuti negli input) e quelli ritenuti importanti dall'algoritmo per risolvere il passo corrente (contenuti negli indizi).

La funzione \(f_m\) consiste in un \gls{mlp} a tre strati e con la \gls{relu} come funzione di attivazione \(\sigma\):
\begin{equation}
  f_m(\bm{z}_i, \bm{z}_j, \bm{e}_{ij}, \bm{g}) = \sigma\left(\bm{W}^{(3)}\sigma\left(\bm{W}^{(2)}\sigma\left(\bm{W}^{(1)}\left[\bm{z}_i||\bm{z}_j||\bm{e}_{ij}||\bm{g}\right] + \bm{b}^{(1)}\right) + \bm{b}^{(2)}\right) + \bm{b}^{(3)}\right)
\end{equation}

Invece la funzione di lettura \(f_r\) è implementata tramite un singolo strato lineare che a sua volta usa la \gls{relu} come funzione di attivazione:
\begin{equation}
  f_r\left(\bm{z}_i, \bm{m}_i\right) = \sigma\left(\bm{W}\left[\bm{z}_i||\bm{m}_i\right] + \bm{b}\right)
\end{equation}

\subsubsection{Meccanismo di Gating\label{subsub:gating_mechanism}}

Per come è definito il processore (\cref{eq:processor_gnn}) esso tende ad aggiornare lo stato interno di tutti i nodi ad ogni sua esecuzione.
Tuttavia molti algoritmi aggiornano solo una parte del loro stato interno alla volta e così dovrebbe fare anche il processore, per aderire meglio al procedimento che sta imparando.
Per favorire questo viene introdotta una funzione di gating \(f_g\)\footnote{\(f_g : \mathbb{R}^{2h} \times \mathbb{R}^{h} \rightarrow \mathbb{R}^{h}\)} che calcola un vettore \(\bm{g}^{(t)}_i\) per ogni nodo del grafo.
Il suddetto vettore controlla quali informazioni contenute del vettore \(\bm{h}^{(t)}_i\) verranno usate durante il prossimo passo e quali verranno scartate.
Come \(f_g\) viene impiegato un \gls{mlp} a due strati avente come prima funzione di attivazione la \gls{relu} e che applica la funzione logistica al risultato del secondo strato per ottenere un risultato compreso tra 0 e 1:
\begin{equation}
  \bm{g}^{(t)} = f_g\left(\bm{z}^{(t)}_i, \bm{m}^{(t)}_i\right) = \text{Logistic}\left(\bm{W}^{(2)}\sigma\left(\bm{W}^{(1)}\left[\bm{z}^{(t)}_i||\bm{m}^{(t)}_i\right] + \bm{b}^{(1)}\right) + \bm{b}^{(2)}\right)
\end{equation}
Per incentivare la funzione di gating a mantenere lo stato precedente il termine \(b^{(2)}\) è inizializzato con il valore -3 invece che con 0.

Successivamente gli embeddings sono così combinati per ottenere il prossimo stato \(\widehat{\bm{h}}^{(t)}_i\):
\begin{equation}
  \widehat{\bm{h}}^{(t)}_i = \bm{g}^{(t)}_i \odot \bm{h}^{(t)}_i + \left(1 - \bm{g}^{(t)}_i\right) \odot \bm{h}^{(t-1)}_i
\end{equation}
che va a rimpiazzare \(\bm{h}^{(t)}_i\) nella \cref{eq:processor_gnn} che quindi diventa \(\bm{z}^{(t)}_i = \bm{x}^{(t)}_i||\widehat{\bm{h}}^{(t-1)}_i\).

\subsubsection{Ragionamento su triplette di nodi\label{subsub:triplet_reasoning}}

Molti algoritmi lavorano su grafi con archi etichettati e aggiornano queste etichette a seconda dei valori associati ai vari archi.
Tuttavia i processori si limitano ad effettuare uno scambio di messaggi tra nodi, rendendo più difficile ragionare sugli archi.
Per estendere il processore e consentire lo scambio di messaggi tra gli archi viene introdotta la funzione \(\psi_t\), la quale genera un vettore per ogni tripletta di nodi del grafo a partire dagli embeddings pertinenti.
Successivamente applicando la massimizzazione, con rispetto ad uno dei nodi, otteniamo lo stato interno degli archi \(\bm{h}_{ij}\):
\begin{equation}
  \bm{t}_{ijk} = \psi_t\left(\bm{z}_i, \bm{z}_j, \bm{z}_k, \bm{e}_{ij}, \bm{e}_{jk}, \bm{e}_{ki}, \bm{g}\right) \quad \bm{h}_{ij} = \phi_t\left(\underset{k}{\texttt{max}}\ \bm{t}_{ijk}\right)
\end{equation}
Entrambe le funzioni sono dei semplici strati lineari che utilizzano la \gls{relu} come funzione di attivazione:
\begin{equation}
  \begin{gathered}
    \psi_t\left(\bm{z}_i, \bm{z}_j, \bm{z}_k, \bm{e}_{ij}, \bm{e}_{jk}, \bm{e}_{ki}, \bm{g}\right)
    = \sigma\left(\bm{W}\left[\bm{z}_i || \bm{z}_j || \bm{z}_k || \bm{e}_{ij} || \bm{e}_{ik} || \bm{e}_{kj} || \bm{g}\right] + \bm{b}\right) \\
    \phi_t\left(\bm{t}_{ij}\right) = \sigma\left(\bm{W}\bm{t}_{ij} + \bm{b}\right)
  \end{gathered}
\end{equation}
Per ridurre l'impatto sulle prestazioni e sull'utilizzo di memoria è consigliabile definire \(\psi_t\) perché calcoli delle rappresentazioni di dimensione ridotta, per poi lasciare che sia \(\phi_t\) a proiettarle nello spazio di dimensione corretta.
Lo stato \(\bm{h}_{ij}\) così ottenuto è poi processato insieme ai valori \(\bm{e}_{ij}\), situati negli archi, durante fase di decodifica.

\textbf{Message-passing Neural Networks} \cite{GNNs} che sono un caso particolare delle pointer graph networks ed esattamente come le suddette supportano il gating e lo scambio di messaggi tra archi.
L'unica differenza sta nel come sono definiti i vicinati dei nodi, per i quali si ha \(\mathcal{N}_i = \{1, \dots, n\}\).

\textbf{Deep Sets} \cite{deep_sets} dove i nodi del grafo sono connessi solamente a se stessi: \(\mathcal{N}_i = \{i\}\).
Dal momento che ogni nodo riceve un singolo messaggio la funzione \(\bigoplus\) è irrilevante e l'equazione (\ref{eq:processor_gnn}) può essere riscritta così:
\begin{equation}
  \begin{gathered}
    \bm{z}_i^{(t)} = \bm{x}_i^{(t)}||\bm{h}_i^{(t-1)} \qquad \bm{m}_{i}^{(t)} = f_m\left(\bm{z}_i^{(t)}, \bm{e}_{ii}^{(t)}, \bm{g}^{(t)}\right) \qquad \bm{h}_i^{(t)} = f_r\left(\bm{z}_i^{(t)}, \bm{m}_i^{(t)}\right)
  \end{gathered}
\end{equation}
Come \(f_m\) e \(f_r\) vengono utilizzati rispettivamente un \gls{mlp} con tre strati e un singolo strato lineare, entrambi con la \gls{relu} come funzione di attivazione.

\textbf{Memory Networks} \cite{memnets} le quali sono originariamente pensate per lavorare sul linguaggio naturale (natural language processing).
Al modello viene richiesto di memorizzare una serie di frasi denominate ``storie'' e successivamente gli viene mostrata un'altra frase, che chiamiamo ``query'', contenente una domanda alla quale deve rispondere sulla base delle informazioni memorizzate.
In CLRS questa architettura è riadattata per lavorare sui grafi usando come query l'embedding \(\bm{x}^t_i\) e come storie gli embeddings situati negli archi \(\bm{h}^{(t)}_{ij}\).
Ogni embedding rappresenta una frase e ogni entrata del suddetto vettore una parola all'interno della frase.
La risposta alla domanda è il nuovo stato dell'\(i\)-esimo nodo \(\bm{h}^{(t)}_i\).

Per costruire la memoria composta da vettori \(\bm{m}_i\), ogni storia è sottoposta ad un'operazione di embedding che proietta la frase in uno spazio latente di dimensione \(d\).
Ogni parola è mappata nello spazio latente tramite una matrice di embedding \(\bm{A}\) di dimensione \(d \times V\)\footnote{Nella pubblicazione originale \(V\) è la dimensione del vocabolario conosciuto dal modello. Invece in CLRS si ha \(V = h\).} e successivamente sono sommate tra di loro, per formare un singolo embedding codificante l'intera frase.
Lo stesso procedimento è applicato durante la generazione dello stato interno \(\bm{u}\) e degli output embeddings \(\bm{c}_i\), utilizzando altre due matrici di embedding, rispettivamente \(\bm{B}\) e \(\bm{C}\), della stessa dimensione di \(\bm{A}\).
\begin{equation}
    \bm{m}_i = \sum_j \bm{A}_{\bm{s}_{ij}} \quad
    \bm{u} = \sum_j \bm{B}_{\bm{q}_j} \quad
    \bm{c}_i = \sum_j \bm{C}_{\bm{s}_{ij}}
\end{equation}
dove \(\bm{s}_{ij}\) è la \(j\)-esima parola della \(i\)-esima storia e \(\bm{q}_j\) è la \(j\)-esima parola della query.
Questo procedimento rappresenta ogni frase tramite una semplice \gls{bow}, la quale, tuttavia, non cattura l'ordine delle parole ma solo la loro molteplicità.
Per ovviare a questa limitazione la fase di encoding viene così estesa:
\begin{equation}
  \bm{m}_i = \sum_j \bm{l}_j \odot \bm{A}_{\bm{s}_{ij}} \quad
  \bm{u} = \sum_j \bm{l}_j \odot \bm{B}_{\bm{q}_j} \quad
  \bm{c}_i = \sum_j \bm{l}_j \odot \bm{C}_{\bm{s}_{ij}}
\end{equation}
dove \(\odot\) è la moltiplicazione rispetto ai singoli elementi e \(l_j\) è un vettore così definito \(\bm{l}_{kj} = 1/2\left(\left(1 - j/J\right) - \left(k/d\right)\left(1 - 2j/J\right)\right)\) (assumendo che gli indici partano da 1) nel quale \(J\) è il numero di parole in una frase.
Questa rappresentazione estesa è chiamata position encoding \cite{memnets}.

La fase di embedding è in realtà opzionale e può essere omessa. In tal caso le queries e le storie sono utilizzate senza alcuna modifica e si ha: \(\bm{m}_i = \bm{c}_i = \bm{s}_i\) e \(\bm{u} = \bm{q}\).

Successivamente viene calcolata la compatibilità tra \(\bm{u}\) e ogni memoria \(\bm{m}_i\):
\begin{equation}
  \label{probs}
  \bm{p}_i = \text{softmax}(\bm{u}^{\intercal}\bm{m}_i)
\end{equation}
ottenendo il vettore di probabilità \(\bm{p}_i\).

Il risultato dell'accesso alla memoria è il vettore \(\bm{o}\) calcolato tramite una somma pesata dei valori \(\bm{c}_i\) usando come pesi le probabilità \(p_i\).
\begin{equation}
  \label{layer_out}
  \bm{o} = \sum_i \bm{p}_i\bm{c}_i
\end{equation}

\'E possibile effettuare più accessi consecutivi alla memoria ripetendo i calcoli descritti nelle \cref{probs,layer_out}.
Per ogni passo successivo al primo la memoria e gli output embeddings rimangono invariati, mentre il valore di \(\bm{u}\) è così aggiornato: \(\bm{u}^{(t)} = \bm{H}(\bm{u}^{(t-1)} + \bm{o}^{(t-1)})\), dove \(\bm{H}\) è una matrice di dimensioni \(d \times d\).

Infine, durante l'ultimo passo, viene generato il nuovo stato interno per il nodo:
\begin{equation}
  \bm{h}^{(t)}_i = \bm{W}(\bm{o} + \bm{u})
\end{equation}
dove \(\bm{W}\) è una matrice di dimensioni \(V \times d\), che mappa la risposta dallo spazio latente della MemNet allo spazio latente usato dagli encoders e decoders.

\subsection{Decoder\label{sub:dec}}

Infine lo stato interno di ogni nodo è decodificato dallo specifico decoder \(g_\tau\), per ricavare gli indizi per la prossima iterazione e, durante l'ultimo passo dell'algoritmo, la soluzione al problema.
Similmente all'encoder, \(g_\tau\) è composto da uno strato lineare per ogni indizio e output.

Prima che vengano utilizzati, come input al passo successivo o per calcolare la loss, su ogni output del decoder viene poi applicata una funzione per mappare il loro valore nell'intervallo corretto (quale funzione viene utilizzata dipende dal tipo dell'output).
Quest'operazione è chiamata ``post-processing'' e può essere eseguita in modalità \textit{hard} o \textit{soft} (per gli output è sempre \textit{hard}).
In modalità hard i valori di tipo mask sono sottoposti ad una operazione di sogliatura con soglia 0 e ai valori espressi come vettori one-hot (categorici, mask\_one e puntatori) viene applicata la funzione \texttt{argmax}.
Se invece si vuole permettere al gradiente di fluire attraverso gli indizi è necessario usare la modalità soft.
In questo caso al posto della sogliatura viene usata la funzione logistica e l'\texttt{argmax} viene sostituito con la funzione \texttt{softmax}.
In entrambi i casi, sugli scalari non viene imposto nessun vincolo e sono utilizzati senza modifiche.

\subsubsection{L'operatore di Sinkhorn\label{subsub:sinkhorn}}

La soluzione degli algoritmi di ordinamento (Insertion Sort, Bubble Sort, Heapsort e Quicksort) non è altro che una permutazione degli elementi.
Questa permutazione può essere codificata, nel contesto di CLRS, con dei valori \texttt{puntatore}, dove ogni nodo punta al proprio predecessore (il primo nodo punta a se stesso).
L'intero ordinamento è esprimibile tramite una matrice \(\bm{P}\) di dimensione \(n \times n\) dove ogni riga è un vettore one-hot e la generica entrata \((i, j)\) ha valore 1 se il nodo \(i\) punta al nodo \(j\).
Tuttavia questa codifica non fornisce alcun aiuto nell'apprendere che l'output dovrebbe rappresentare una permutazione.
Per indirizzare il modello nella giusta direzione viene modificato il decoder responsabile dell'output degli algoritmi di ordinamento, perché sfrutti il che i puntatori codificano una permutazione.
Per prima cosa si deve modificare la codifica dell'output, facendo puntare il primo nodo, non più a se stesso, ma all'ultimo.
Così facendo \(\bm{P}\) diventa una matrice di permutazione, cioè una matrice le cui righe e colonne sono vettori one-hot.
Inoltre viene aggiunto un altro output situato nei nodi e di tipo \texttt{mask\_one} che indica qual'è il primo elemento nella lista.
Poi è necessario cambiare come viene trattato l'output del decoder durante la fase di post-processing.
La matrice di permutazione \(\bm{P}\) è calcolata a partire dall'output \(\bm{Y}\) sostituendo la funzione softmax, applicata ad ogni riga, con l'operatore di Sinkhorn \(\mathcal{S}\) \cite{positive_sinkhorn, nonnegative_sinkhorn,visual_permutation, optimal_transport, gumbel_sinkhorn}. % MAYBE: change the ordering of references because of this
\(\mathcal{S}\) trasforma una qualunque matrice quadrata \(\bm{Y}\) in una matrice bistocastica (una matrice non negativa le cui righe e colonne hanno somma 1), esponenziando i valori di \(\bm{Y}\) e poi normalizzando ripetutamente le righe e le colonne così che la loro somma sia 1.
La definizione di \(\mathcal{S}\) è la seguente:
\begin{equation}
  \mathcal{S}^{(0)}(\bm{Y}) = \exp(\bm{Y}) \quad \mathcal{S}^{(l)}(\bm{Y}) = \mathcal{T}_c(\mathcal{T}_r(\mathcal{S}^{(l-1)}(\bm{Y}))) \quad \mathcal{S}(\bm{Y}) = \lim_{l \rightarrow \infty} \mathcal{S}^{(l)}(\bm{Y})
\end{equation}
dove \(\mathcal{T}_c\) e \(\mathcal{T}_r\) sono la normalizzazione rispetto alle colonne e alle righe.
Sebbene l'operatore di Sinkhorn dia come risultato una matrice bistocastica, è possibile ottenere una matrice di permutazione introducendo una parametro temperatura \(\tau > 0\) e ponendo \(\bm{P} = \lim_{\tau \rightarrow 0^+} \mathcal{S}(\bm{Y}/\tau)\).
Assumendo che ogni colonna e riga di \(\bm{Y}\) abbia una valore massimo univoco, \(\bm{P}\) sarà una matrice di permutazione \cite[Teorema 1]{optimal_transport}.
Per evitare questo problema, esclusivamente durante l'allenamento e subito prima di applicare l'operatore, viene sommato ai valori di \(\bm{Y}\) del rumore, campionato secondo la distribuzione di Gumbel \cite{gumbel_sinkhorn}.
Infine, la matrice \(\bm{P}\) e l'output di tipo \texttt{mask\_one} sono combinati per ottenere la codifica originale.

\section{Limitazioni derivanti dall'utilizzo di JAX\label{sec:jax_limitations}}

Pytorch è più maturo e offre una numero maggiore di funzionalità rispetto a JAX.
Per esempio Pytorch dispone del supporto nativo per la \gls{amp}, la quale usa 16 bit per la rappresentazione dei numeri in virgola mobile coinvolti in alcune operazione, risultando in una tangibile velocizzazione nell'applicazione dei modelli.
D'altra parte se si vuole usare la precisione mista in JAX è necessario implementarla manualmente.
Dal punto di vista dell'hardware, il supporto per le gpus AMD da parte di Pytorch è considerato stabile, mentre nel caso di JAX è ancora sperimentale.
Inoltre Pytorch è più indicato per gestire i modelli una volta raggiunta la fase di produzione.
Ad esempio, tramite TorchServe, è possibile, con relativamente pochi comandi, esporre un modello in rete e rendere disponibili le sue funzionalità attraverso una API di tipo REST.

Implementando un modello in Pytorch si ottiene l'accesso a un vasto ecosistema di librerie\footnote{\url{https://pytorch.org/ecosystem}}.
Anche se JAX ha un ecosistema circostante, non è così esteso e maturo come quello di Pytorch.
Ad esempio, esistono librerie per l'apprendimento basato sulle ricompense per entrambi i frameworks, Stable Baselines per Pytorch e rlax per JAX.
Stable Baselines è molto più maturo di rlax, essendo in sviluppo da quasi il doppio del tempo.
Inoltre, al momento della scrittura di questo testo, Stable Baselines è ancora in fase di sviluppo attivo, mentre rlax non ha subito alcuna modifica in 5 mesi.

Oltre a questo è pur sempre utile considerare il paradigma adottato da JAX.
Può essere difficile lavorare con il paradigma funzionale e il che si traduce in una curva di apprendimento più ripida per JAX rispetto a Pytorch, il quale si sforza di rendere la sua sintassi il più simile possibile a quella di Python.
Pytorch cercare di essere il più Pythonico possibile il che è un punto a suo favore considerando che conoscere Python è già un requisito per il suo utilizzo.
Al contrario, anche se si è già in grado di usare Python, per servirsi di JAX è comunque necessario imparare il diverso approccio da esso impone.
Anche se è vero che utilizzando alcune librerie, come per esempio haiku, è possibile evitare alcuni dei fastidi derivanti dal paradigma funzionale, non è possibile eliminarli tutti.
JAX è un ottimo framework per i ricercatori, ma quando si tratta dell'utilizzo in produzione, il più maturo Pytorch è una scelta migliore.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis"
%%% End:
